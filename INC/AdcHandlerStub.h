
#ifndef ADCSTUB_H
#define ADCSTUB_H


#include "r_cg_userdefine.h"
#include "r_cg_macrodriver.h"


void Adcsetter(void);
void AdcSetterSpecific(uint8_t ch_no, uint16_t raw);

#endif

