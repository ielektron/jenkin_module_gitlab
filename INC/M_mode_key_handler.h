#ifndef M_MODE_KEY_HANDLER_H
#define M_MODE_KEY_HANDLER_H
/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define SYS_MODE_TYPE 3


/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/
typedef enum
{
	EXT_KEY = 0,
	UI_KEY = 1
}MODE_KEY_TYPE;
 
//dont change the order-see s8_G_DNB[] in zone handler
typedef enum
{
	DISARM_MODE = 0,
	PART_ARM_MODE,	
	ARM_MODE,	 	 
	DEFAULT_MODE=4,//it must not be less than 4 because if the key is not connected the value read by gpio is 3
	SYS_MODE_END
}SYS_MODE_E;
 
/***********************************************************************************************************************
Global functions
***********************************************************************************************************************/
/***********************************************************************************************************************
* Function Name: MKEY_create
* Description  : This function initializes the mode key handler
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/

void MKEY_create(void);

/***********************************************************************************************************************
* Function Name: m_vMKEY_monitor
* Description  : This function monitors the mode key and updates ther system mode.
* Arguments    : None
* Return Value : Returns changed mode otherwise DEFAULT_MODE
***********************************************************************************************************************/

SYS_MODE_E m_vMKEY_monitor(void);

/***********************************************************************************************************************
* Function Name: m_u8ModeHdlr_update
* Description  : stores the mode read from eeprom to global structure sys_data
* Arguments    : Eeprrom array
* Return Value : one value of enum G_RESP_CODE
***********************************************************************************************************************/

G_RESP_CODE m_u8ModeHdlr_update(const uint8_t *dat);

/***********************************************************************************************************************
* Function Name: m_u8MKEY_sts_get
* Description  : Returns the current system mode
* Arguments    : None
* Return Value : one value of enum SYS_MODE_E
***********************************************************************************************************************/

SYS_MODE_E m_u8MKEY_sts_get(void);

/***********************************************************************************************************************
* Function Name: m_u8MKEY_sts_set
* Description  : Updates the current system mode
* Arguments    : one value of enum SYS_MODE_E
* Return Value : None
***********************************************************************************************************************/

void m_u8MKEY_sts_set(SYS_MODE_E mode);
 
#endif
