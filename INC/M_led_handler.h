#ifndef M_LED_HANDLER_H
#define M_LED_HANDLER_H

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/
typedef enum
{					//do not change the order
	LED_AC=0,	//IC2_8BITS
	LED_BAT_GOOD,//21
	LED_BAT_BAD,//22
	LED_ARM,
	LED_ALERT,	
	LED_END
}LED_NAME;
 
/***********************************************************************************************************************
* Function Name: LED_create
* Description  : This function initializes the Led handler
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/

void LED_create(void); 

/***********************************************************************************************************************
* Function Name: m_u8LED_ctrl_led
* Description  : This function controls the leds
* Arguments    : LED_NAME - one value of enum LED_NAME
				 cmd  - one value of enum G_CTL_CODES
* Return Value : None
***********************************************************************************************************************/

void m_u8LED_ctrl_led(LED_NAME choice,G_CTL_CODES cmd);
 

#endif