#ifndef M_EEPROM_HANDLER_H_
#define M_EEPROM_HANDLER_H_

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "H_M95M01_eeprom_driver.h" 

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/

#define LOG_START 0 
#define LOG_STP 1

/***********************************************************************************************************************
Extern variables
***********************************************************************************************************************/
extern uint8_t EWrite_DataBuffer[MAX_EEPROM_PAGE_SIZE];

/***********************************************************************************************************************
Global functions
***********************************************************************************************************************/
void E2P_eventManager(E_SYS_EVENTS event,uint8_t sts);
G_RESP_CODE E2P_LOG_STORE(void);
G_RESP_CODE E2P_CRI_STORE(void);
G_RESP_CODE E2P_CRI_RETRIEVE(void);
G_RESP_CODE E2P_SendSYSdata(void);
G_RESP_CODE E2P_SendSYS_ALT_data(E_SYS_EVENTS command);
G_RESP_CODE E2P_ReceiveSYSdata(uint8_t *arr);
void E2P_retryHandler(void);
G_RESP_CODE E2P_LogRetrieve(uint8_t log_cmd);
G_RESP_CODE E2P_LogReset(void);
 
G_RESP_CODE E2P_criticalDataRetrieve(void);
G_RESP_CODE E2P_criticalDataStore(void);
G_RESP_CODE E2P_LogStore(void); 

#ifdef STARTUP_E2P_CLEAR
void m_vE2P_clear(void);
#endif


#endif