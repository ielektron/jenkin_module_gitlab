
/*

filename : gtst_main.cpp
author : Pallavi
created at : 2019-06-08 13:48:03.717862

*/
    

#include <stdio.h>
#include "gtest/gtest.h"

GTEST_API_ int main(int argc, char **argv) {
  printf("Running main() from gtest_main.cc\n");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
        