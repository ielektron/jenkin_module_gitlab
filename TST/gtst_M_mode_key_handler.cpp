
/*

filename : gtst_Jenkin_module.cpp
author : Pallavi
created at : 2019-06-08 13:48:03.730856

*/ 
    
# include "gtest/gtest.h"
//#include "pch.h"

extern "C" { 
	#include "M_mode_key_handler.h"
	#include "M_mode_key_handler.c"
	#include "M_led_handler.h"
	#include "H_GPIO_driver.h" 
	#include "stub.h"
	#include "stub.c" 	
}

class MODE_KEY_TEST : public::testing::Test {
protected:

	void SetUp()
	{
		MKEY_create();
		LED_create();
		//key position arm
		GPIO_input_set(PIN_NAME_MKEY1, GPIO_HIGH);
		GPIO_input_set(PIN_NAME_MKEY2, GPIO_HIGH);
	}
};

TEST_F(MODE_KEY_TEST, KeyDisarmCheck)
{	 
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_LOW);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_HIGH);

	//key debounce count
	m_vMKEY_monitor();
	m_vMKEY_monitor();
	m_vMKEY_monitor();

	EXPECT_EQ(DISARM_MODE, m_u8MKEY_sts_get());	
}
TEST_F(MODE_KEY_TEST, KeyArmCheck)
{
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_HIGH);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_HIGH);

	//key debounce count
	m_vMKEY_monitor();
	m_vMKEY_monitor();
	m_vMKEY_monitor();


	EXPECT_EQ(ARM_MODE, m_u8MKEY_sts_get());

	EXPECT_EQ(LED_ARM, get_led_name());//armed mode arm led will be switched on
	EXPECT_EQ(ON, get_led_sts());
}
TEST_F(MODE_KEY_TEST, KeyPartArmCheck)
{
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_HIGH);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_LOW);

	//key debounce count
	m_vMKEY_monitor();
	m_vMKEY_monitor();
	m_vMKEY_monitor();


	EXPECT_EQ(PART_ARM_MODE, m_u8MKEY_sts_get());

	EXPECT_EQ(LED_ARM, get_led_name());
	EXPECT_EQ(ON, get_led_sts());
}

TEST_F(MODE_KEY_TEST, TransitionFromDisarmToPartArm)
{
	/**m_vMKEY_monitor() will be called periodically every 500ms**/

	/**********precondition******/
	m_u8MKEY_sts_set(DISARM_MODE);

	/**********Action************/
	/***wait for 500ms and change the key to part arm *****/
	m_vMKEY_monitor();
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_HIGH);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_LOW);

	/**********Verify************/
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count1
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count2
	EXPECT_EQ(PART_ARM_MODE, m_vMKEY_monitor());//count3 mode changed count0

	EXPECT_EQ(PART_ARM_MODE, m_u8MKEY_sts_get());
}

TEST_F(MODE_KEY_TEST, TransitionFromDisarmToArm)
{
	/**m_vMKEY_monitor() will be called periodically every 500ms**/

	/**********precondition******/
	//key position disarm
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_LOW);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_HIGH);
	m_u8MKEY_sts_set(DISARM_MODE);	 

	/**********Action************/
	/***wait for 500ms and change the key to arm *****/
	m_vMKEY_monitor();
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_HIGH);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_HIGH);

	/**********Verify************/
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count1
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count2
	EXPECT_EQ(ARM_MODE, m_vMKEY_monitor());//count3 mode changed count0

	EXPECT_EQ(ARM_MODE, m_u8MKEY_sts_get());
}

TEST_F(MODE_KEY_TEST, TransitionFromDisarmToDisarm)
{
	/**m_vMKEY_monitor() will be called periodically every 500ms**/

	/**********precondition******/
	m_u8MKEY_sts_set(DISARM_MODE);

	/**********Action************/
	/***wait for 500ms and change the key to Disarm *****/
	m_vMKEY_monitor();
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_LOW);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_HIGH);

	/**********Verify************/
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count1
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count2
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count3 mode changed count0

	EXPECT_EQ(DISARM_MODE, m_u8MKEY_sts_get());
}

TEST_F(MODE_KEY_TEST, TransitionFromPartArmToDisarm)
{
	/**m_vMKEY_monitor() will be called periodically every 500ms**/

	/**********precondition******/ 
	m_u8MKEY_sts_set(PART_ARM_MODE);

	/**********Action************/
	/***wait for 500ms and change the key to part arm *****/
	m_vMKEY_monitor();
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_LOW);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_HIGH);

	/**********Verify************/
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count1
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count2
	EXPECT_EQ(DISARM_MODE, m_vMKEY_monitor());//count3 mode changed count0

	EXPECT_EQ(DISARM_MODE, m_u8MKEY_sts_get());
}

TEST_F(MODE_KEY_TEST, TransitionFromPartArmToArm)
{
	/**m_vMKEY_monitor() will be called periodically every 500ms**/

	/**********precondition******/
	//key position - PART ARM
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_HIGH);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_LOW);
	m_u8MKEY_sts_set(PART_ARM_MODE);

	/**********Action************/
	/***wait for 500ms and change the key to part arm *****/
	m_vMKEY_monitor();
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_HIGH);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_HIGH);

	/**********Verify************/
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count1
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count2
	EXPECT_EQ(ARM_MODE, m_vMKEY_monitor());//count3 mode changed count0

	EXPECT_EQ(ARM_MODE, m_u8MKEY_sts_get());
}

TEST_F(MODE_KEY_TEST, TransitionFromPartArmToPartArm)
{
	/**m_vMKEY_monitor() will be called periodically every 500ms**/

	/**********precondition******/	 
	m_u8MKEY_sts_set(PART_ARM_MODE);

	/**********Action************/
	/***wait for 500ms and change the key to part arm *****/
	m_vMKEY_monitor();
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_HIGH);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_LOW);

	/**********Verify************/
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count1
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count2
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count3 mode changed count0

	EXPECT_EQ(PART_ARM_MODE, m_u8MKEY_sts_get());
}
TEST_F(MODE_KEY_TEST, TransitionFromArmToPartArm)
{
	/**m_vMKEY_monitor() will be called periodically every 500ms**/

	/**********precondition******/
	m_u8MKEY_sts_set(ARM_MODE);

	/**********Action************/
	/***wait for 500ms and change the key to part arm *****/
	m_vMKEY_monitor();
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_HIGH);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_LOW);

	/**********Verify************/
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count1
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count2
	EXPECT_EQ(PART_ARM_MODE, m_vMKEY_monitor());//count3 mode changed count0

	EXPECT_EQ(PART_ARM_MODE, m_u8MKEY_sts_get());
}
TEST_F(MODE_KEY_TEST, TransitionFromArmToDisarm)
{
	/**m_vMKEY_monitor() will be called periodically every 500ms**/

	/**********precondition******/
	m_u8MKEY_sts_set(ARM_MODE);

	/**********Action************/
	/***wait for 500ms and change the key to part disarm *****/
	m_vMKEY_monitor();
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_LOW);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_HIGH);

	/**********Verify************/
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count1
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count2
	EXPECT_EQ(DISARM_MODE, m_vMKEY_monitor());//count3 mode changed count0

	EXPECT_EQ(DISARM_MODE, m_u8MKEY_sts_get());
}

TEST_F(MODE_KEY_TEST, TransitionFromArmToArm)
{
	/**m_vMKEY_monitor() will be called periodically every 500ms**/

	/**********precondition******/
	//key position disarm
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_LOW);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_HIGH);
	m_u8MKEY_sts_set(ARM_MODE);

	/**********Action************/
	/***wait for 500ms and change the key to part arm *****/
	m_vMKEY_monitor();
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_HIGH);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_HIGH);

	/**********Verify************/
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count1
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count2
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count3 mode changed count0

	EXPECT_EQ(ARM_MODE, m_u8MKEY_sts_get());
}
TEST_F(MODE_KEY_TEST, InvalidInput)
{
	/**m_vMKEY_monitor() will be called periodically every 500ms**/

	/**********precondition******/	 
	m_u8MKEY_sts_set(ARM_MODE);

	/**********Action************/
	/***wait for 500ms and give invalid input*****/
	m_vMKEY_monitor();
	GPIO_input_set(PIN_NAME_MKEY1, GPIO_LOW);
	GPIO_input_set(PIN_NAME_MKEY2, GPIO_LOW);


	/**********Verify************/
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count1
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count2
	EXPECT_EQ(DEFAULT_MODE, m_vMKEY_monitor());//count3 mode changed count0

	EXPECT_EQ(ARM_MODE, m_u8MKEY_sts_get()); 
}


