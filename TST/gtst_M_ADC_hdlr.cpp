
/*

filename : gtst_Jenkin_module.cpp
author : Pallavi
created at : 2019-06-08 13:48:03.730856

*/
# include "gtest/gtest.h"
extern "C"
{
#include "M_ADC_hdlr.h"
#include "M_ADC_hdlr.c"
#include "AdcHandlerStub.h"
#include "AdcHandlerStub.c"
}

TEST(M_ADC_ConditionerTest, RequirementTestName1) 
{	//testing channel 0 buffer value
	uint16_t buffer = 0;
	M_ADC_init();
	Adcsetter();
	AdcSetterSpecific(0, 10);
	M_ADC_Conditioner();
	ASSERT_EQ(ADC_STABLE,M_ADC_CHsts_get(0, &buffer));
	ASSERT_EQ(451, buffer);
}
// RequirementTest Positive Testcase 
TEST(M_ADC_ConditionerTest, RequirementTestName2)
{	//testing channel 1 buffer value
	uint16_t buffer = 0;
	M_ADC_init();
	Adcsetter();
	AdcSetterSpecific(1, 10);
	M_ADC_Conditioner();
	ASSERT_EQ(ADC_STABLE, M_ADC_CHsts_get(1, &buffer));
	ASSERT_EQ(451, buffer);
}
// RequirementTest Positive Testcase 
TEST(M_ADC_ConditionerTest, RequirementTestName3)
{	//testing channel 1 buffer value
	uint8_t i;
	uint16_t buffer = 0;
	M_ADC_init();
	Adcsetter();
	AdcSetterSpecific(1, 10);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	M_ADCMonitor();
	M_ADCMonitor();
	M_ADCMonitor();
	ASSERT_EQ(ADC_STABLE, M_ADC_CHsts_get(1, &buffer));
	ASSERT_EQ(10, buffer);
}
// RequirementTest Positive Testcase
TEST(M_ADC_ConditionerTest, RequirementTestName4)
{	//testing channel 1 buffer value
	uint8_t i;
	uint16_t buffer = 0;
	M_ADC_init();
	Adcsetter();
	AdcSetterSpecific(1, 10);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1, 11);;
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,12);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,13);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	ASSERT_EQ(ADC_STABLE, M_ADC_CHsts_get(1, &buffer));
	ASSERT_EQ(13, buffer);
}

// RequirementTest Negative Testcase 
TEST(M_ADC_ConditionerTest, ReqiurementTestName5)
{	//testing channel 1 buffer value
	uint8_t i;
	uint16_t buffer = 0;
	M_ADC_init();
	Adcsetter();
	AdcSetterSpecific(1,10);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	M_ADCMonitor();
	AdcSetterSpecific(1,100);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	M_ADCMonitor();
	ASSERT_EQ(ADC_UNSTABLE, M_ADC_CHsts_get(1, &buffer));
	//ASSERT_EQ(10, buffer);
}

// RequirementTest Negative Testcase 
TEST(M_ADC_ConditionerTest, RequirementTestName6)
{	//testing channel 1 buffer value
	uint8_t i;
	uint16_t buffer = 0;
	M_ADC_init();
	Adcsetter();
	AdcSetterSpecific(1,10);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,11);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,12);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,21);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	ASSERT_EQ(ADC_UNSTABLE, M_ADC_CHsts_get(1, &buffer));
	//ASSERT_EQ(13, buffer);
}

//LogicalBoundaryTest 
TEST(M_ADC_ConditionerTest, LogicalBoundaryValueTest)
{	//testing channel 1 buffer value
	uint8_t i;
	uint16_t buffer = 0;
	M_ADC_init();
	Adcsetter();
	AdcSetterSpecific(1,1025);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,1025);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,1025);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,1025);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	ASSERT_EQ(ADC_STABLE, M_ADC_CHsts_get(1, &buffer));
	ASSERT_EQ(0, buffer);
}


//BoundaryTest Minimum Value Test
TEST(M_ADC_ConditionerTest, MinimumValueTest)
{	//testing channel 1 buffer value
	uint8_t i;
	uint16_t buffer = 0;
	M_ADC_init();
	Adcsetter();
	AdcSetterSpecific(1,0);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,0);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,0);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,0);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	ASSERT_EQ(ADC_STABLE, M_ADC_CHsts_get(1, &buffer));
	ASSERT_EQ(0, buffer);
}

//BoundaryTest Maximum Value(6553) Test
TEST(M_ADC_ConditionerTest, MaximumValueTest)
{	//testing channel 1 buffer value
	uint8_t i;
	uint16_t buffer = 0;
	M_ADC_init();
	Adcsetter();
	AdcSetterSpecific(1,(uint16_t)6553);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,(uint16_t)6553);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,(uint16_t)6553);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,(uint16_t)6553);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	ASSERT_EQ(ADC_STABLE, M_ADC_CHsts_get(1, &buffer));
	ASSERT_EQ((uint16_t)6553, buffer);
}
//BoundrayTest value rollover
TEST(M_ADC_ConditionerTest, RollOverTest)
{	//testing channel 1 buffer value
	uint8_t i;
	uint16_t buffer = 0;
	M_ADC_init();
	Adcsetter();
	AdcSetterSpecific(1,(uint16_t)65536);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,(uint16_t)65536);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,(uint16_t)65536);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,(uint16_t)65536);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	ASSERT_EQ(ADC_STABLE, M_ADC_CHsts_get(1, &buffer));
	ASSERT_EQ(0, buffer);
}
//BoundaryTest Negative TestCase
TEST(M_ADC_ConditionerTest, BoundaryTest)
{	//testing channel 1 buffer value
	uint8_t i;
	uint16_t buffer = 0;
	M_ADC_init();
	Adcsetter();
	AdcSetterSpecific(1,(uint16_t)6554);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,(uint16_t)6554);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,(uint16_t)6554);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	AdcSetterSpecific(1,(uint16_t)6554);
	for (i = 0; i < 10; i++)
	{
		M_ADC_Conditioner();
	}
	M_ADCMonitor();
	ASSERT_EQ(ADC_STABLE, M_ADC_CHsts_get(1, &buffer));
	ASSERT_NE((uint16_t)6554, buffer);
}